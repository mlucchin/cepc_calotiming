#ifndef CreateTree_H
#define CreateTree_H 1

#include <iostream>
#include <vector>
#include <map>
#include "TString.h"

#include "TH2F.h"
#include "TProfile2D.h"
#include "TH3F.h"
#include "TFile.h"
#include "TTree.h"
#include "TNtuple.h"

class CreateTree
{
private:
  
  TTree*  ftree ;
  TString fname ;
  
public:
  
  CreateTree (TString name);
  ~CreateTree () ;
  
  TTree*          GetTree() const { return ftree; };
  TString         GetName() const { return fname; };
  void             AddEnergyDeposit(int index, float deposit);
  void             AddScintillationPhoton(int index);
  void             AddCerenkovPhoton(int index);
  int                Fill();
  bool             Write(TFile *);
  void             Clear() ;
  
  static CreateTree* Instance() { return fInstance; } ;
  static CreateTree* fInstance;
  
  int Event;

  float inputTrackerX0;
  float inputServiceAlmm;
  float inputTimingThick;
  float inputE1Thick;
  float inputE2Thick;
  float inputE1Width;
  float inputQuartzSize;
  float inputTimingECAL_dist;

  
  float inputMomentum[4] ; // Px Py Pz E
  float inputInitialPosition[3] ; // x, y, z

  float primaryMomT1[4] ; // Px Py Pz E
  float primaryPosT1[3] ; // x, y, z

  float primaryMomE1[4] ; // Px Py Pz E
  float primaryPosE1[3] ; // x, y, z

  int nTracksT1;
  int nTracksT2;
  int nTracksE1;
  int nTracksE2;
  int nTracksTRK[6];

  //integrated energy in each longitudinal layer
  float depositedEnergyTotal;
  float depositedEnergyTotTRKVolume;
  float depositedEnergyTiming_f;
  float depositedEnergyTiming_r;
  float depositedEnergyECAL_f;
  float depositedEnergyECAL_r;
  float depositedEnergyQuartz;
  float depositedEnergySolenoid;
  float depositedEnergyServices;
  float depositedEnergyHCAL;
  float depositedEnergyHCAL_ScintOnly;

  float nonIonDepEnergyTotTRKVolume;
  float nonIonDepEnergyTiming_f;
  float nonIonDepEnergyTiming_r;
  float nonIonDepEnergyECAL_f;
  float nonIonDepEnergyECAL_r;
  float nonIonDepEnergyQuartz;
  float nonIonDepEnergyHCAL;

  int tot_phot_cer_Timing_f;
  int tot_phot_cer_Timing_r;
  int tot_phot_cer_ECAL_f ;
  int tot_phot_cer_ECAL_r ;
  int tot_phot_cer_Quartz ;
  int tot_phot_cer_HCAL;

  float depositedEnergyWorld;

  float Edep_Tracker_layer[6];

  //energy deposit in each trasnversally segmented channel
  float Edep_Timing_f_ch[18];
  float Edep_Timing_r_ch[18];
  float Edep_ECAL_f_ch[400];
  float Edep_ECAL_r_ch[400];
  float Edep_HCAL_layer[50];
  float nonIonEdep_HCAL_layer[50];

  // std::vector<float> *gamma_brem; 
  // std::vector<float> *ele_ioni; 

//  std::vector<float> E_dep_crystal;

  /*
  std::vector<float> E_dep_f;
  std::vector<float> E_dep_time_f;
  std::vector<float> E_dep_r;
  std::vector<float> E_dep_time_r;
  
  std::vector<float> time_ext_scint;
  std::vector<float> time_ext_cher;
  std::vector<float> time_prod_scint;
  std::vector<float> time_prod_cher;

  std::vector<float> lambda_ext_scint;
  std::vector<float> lambda_ext_cher;
  std::vector<float> lambda_prod_scint;
  std::vector<float> lambda_prod_cher;
  
  std::vector<float> time_ext_scint_ref;
  std::vector<float> time_ext_cher_ref;
  std::vector<float> time_prod_scint_ref;
  std::vector<float> time_prod_cher_ref;
  */


  TH1F* h_phot_cer_lambda_Timing_f ;
  TH1F* h_phot_cer_lambda_Timing_r;
  TH1F* h_phot_cer_lambda_ECAL_f ;
  TH1F* h_phot_cer_lambda_ECAL_r;
  TH1F* h_phot_cer_lambda_Quartz;
  TH1F* h_phot_cer_lambda_HCAL;

  TH2F* h_Edep_crystal;
  TH2F* h_Edep_active;
  TH2F* h_Edep_total;

  TH2F* h_Edep_crystal_front_XY;
  TH2F* h_Edep_crystal_rear_XY;
  TH2F* h_Edep_active_XY;
  TH2F* h_Edep_total_XY;

  /*


  int tot_phot_sci;
  int tot_phot_cer;
  int tot_latGap_phot_sci;
  int tot_latGap_phot_cer;
  int tot_gap_phot_sci;
  int tot_gap_phot_cer;
  int tot_det_phot_sci;
  int tot_det_phot_cer;
  
  TH1F* h_phot_sci_lambda;
  TH1F* h_phot_sci_E;
  TH1F* h_phot_sci_time;
  TH1F* h_phot_sci_angleAtProduction;
  TH1F* h_phot_cer_lambda;
  TH1F* h_phot_cer_E;
  TH1F* h_phot_cer_time;
  TH1F* h_phot_cer_angleAtProduction;
  
  TH1F* h_phot_sci_gap_lambda;
  TH1F* h_phot_sci_gap_E;
  TH1F* h_phot_sci_gap_time;
  TH1F* h_phot_sci_gap_angleAtProduction;
  TH1F* h_phot_sci_gap_angleWithSurfNormal;
  TH1F* h_phot_cer_gap_lambda;
  TH1F* h_phot_cer_gap_E;
  TH1F* h_phot_cer_gap_time;
  TH1F* h_phot_cer_gap_angleAtProduction;
  TH1F* h_phot_cer_gap_angleWithSurfNormal;

  TH1F* h_phot_sci_det_lambda;
  TH1F* h_phot_sci_det_E;
  TH1F* h_phot_sci_det_time;
  TH1F* h_phot_sci_det_angleAtProduction;
  TH1F* h_phot_sci_det_angleWithSurfNormal;
  TH1F* h_phot_cer_det_lambda;
  TH1F* h_phot_cer_det_E;
  TH1F* h_phot_cer_det_time;
  TH1F* h_phot_cer_det_angleAtProduction;
  TH1F* h_phot_cer_det_angleWithSurfNormal;
  */

};

#endif
